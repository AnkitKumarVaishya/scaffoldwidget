import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Learning The Scaffold",
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int value = 1;
  void valueadd() {
    setState(() {
      value++;
    });
  }

  void valuesub() {
    setState(() {
      value = value - 1;
    });
  }

  String date = DateTime.now().toString();
  void dateshow() {
    setState(() {
      date = DateTime.now().toString();
    });
  }

  String data = "No Action";
  void datacall(String value) {
    setState(() {
      data = value;
    });
  }

  List<BottomNavigationBarItem> _items;
  String val = "";
  int index = 0;
  void initState() {
    _items = new List();
    _items.add(
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        title: Text("Home"),
      ),
    );
    _items.add(
      BottomNavigationBarItem(
        icon: Icon(Icons.people),
        title: Text("People"),
      ),
    );
    _items.add(
      BottomNavigationBarItem(
        icon: Icon(Icons.weekend),
        title: Text("Weekend"),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Learning Scaffold"),
        actions: [
          IconButton(icon: Icon(Icons.add), onPressed: () => valueadd()),
          IconButton(icon: Icon(Icons.remove), onPressed: () => valuesub()),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => dateshow(),
        child: Icon(Icons.watch),
        // mini: true,
      ),
      drawer: Drawer(
        child: Container(
          padding: EdgeInsets.all(35.0),
          child: Column(children: [
            Text("Hello This is a drawer"),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Close Drawer"),
            ),
          ]),
        ),
      ),
      persistentFooterButtons: [
        IconButton(
          icon: Icon(Icons.add_a_photo),
          onPressed: () => datacall("Button1"),
        ),
        IconButton(
          icon: Icon(Icons.add_a_photo_outlined),
          onPressed: () => datacall("Button2"),
        ),
        IconButton(
          icon: Icon(Icons.add_a_photo_rounded),
          onPressed: () => datacall("Button3"),
        ),
      ],
      bottomNavigationBar: new BottomNavigationBar(
        items: _items ,
      currentIndex: index,
      fixedColor: Colors.red,
      unselectedItemColor: Colors.blue,
      onTap: (int item){
        setState(() {
          index = item;
          val = "Current index is ${index.toString()}" ;
        });
      }
      ),
      body: Column(
        children: [
          Center(
            child: Text(
              value.toString(),
              style: TextStyle(color: Colors.red, fontSize: 50.0),
            ),
          ),
          Center(
            child: Text(
              date,
              style: TextStyle(color: Colors.red, fontSize: 25.0),
            ),
          ),
          Text(data),
          Text(val),
        ],
      ),
    );
  }
}
